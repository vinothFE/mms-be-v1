const Config = {
  CUSTOMER_API: "/api",
  USER_API: "/api/user",
  INVOICE_API: "/api/invoice",
  STATISTICS_API: "/api/statistics"
};
module.exports = Config;
