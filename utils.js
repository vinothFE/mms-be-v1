const moment = require("moment");
const Utils = {
  getMonthAndYear(date = null) {
    if (date) {
      return moment(date).format("YYYY-MM");
    }
    return moment().format("YYYY-MM");
  },
  getLastMonth() {
    return moment()
      .subtract(1, "months")
      .format("YYYY-MM");
  },
  sumOfArrayObject(milkAmountList) {
    return milkAmountList.reduce((a, { amount }) => a + amount, 0);
  },
  daysInThisMonth() {
    return moment().daysInMonth();
  },
  daysInLastMonth() {
    return moment()
      .subtract(1, "months")
      .daysInMonth();
  },
  currentTimeStamp() {
    return moment();
  }
};

module.exports = Utils;
