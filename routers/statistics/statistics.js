const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const customerSchema = require("../customers/model");
const Customer = mongoose.model("customers", customerSchema);
const _utils = require("../../utils");

/*
    1. Monthly : 
                - Total amount pay - 
                - Collected amount
    2. Supplier: 
                - Total amount
                - Collected amount
                - Rest of amount need to be collect
*/
router.get("/", (req, res) => {
  getCustomersStatisticData().then((cusData, err) => {
    if (err) {
      return;
    }
    if (cusData) {
      result = {};
      Array.from(cusData.map(item => item.supplier)).forEach(supplier => {
        customerData = cusData.filter(data => data["supplier"] === supplier);
        result[supplier] = getStatisticsData(customerData);
      });
      result["all"] = getStatisticsData(cusData);
      res.send(result);
    }
  });
});
router.get("/:supplier", (req, res) => {
  console.log("SUto /n", req.params.supplier);

  getCustomersStatisticData(req.params.supplier).then((cusData, err) => {
    if (err) {
      return;
    }
    if (cusData) {
      res.send([getStatisticsData(cusData)]);
    }
  });
});

function getStatisticsData(cusData) {
  totalCardAmount = 0;
  totalAmountPay = 0;
  totalcollectedAmount = 0;
  cusData.forEach((customer, index) => {
    milkCount = customer["milk_count"] ? customer["milk_count"] : 0;
    totalCardAmount += _utils.daysInThisMonth() * milkCount;
    totalAmountPay += findTotalPayAmount(customer["invoice"]["due"]);
    // console.log("totalAmountPay", totalAmountPay);
    totalcollectedAmount += customer["invoice"]["history"].length
      ? findColletedAmount(customer["invoice"]["history"])
      : 0;
  });

  return {
    totalCardAmount: totalCardAmount,
    totalAmountPay: totalAmountPay,
    totalcollectedAmount: totalcollectedAmount
  };
}

function findTotalPayAmount(dueArray) {
  filterData = dueArray.filter(list => {
    return _utils.getMonthAndYear(list.timestamp) === _utils.getMonthAndYear();
  });
  if (filterData.length) {
    return filterData[filterData.length - 1]["amount"];
  }
  return 0;
}

function findColletedAmount(historyArray) {
  // console.log("historyArray array :", historyArray);
  collectedAmount = Array.from(historyArray).filter(list => {
    return _utils.getMonthAndYear(list.timestamp) === _utils.getMonthAndYear();
  });
  return _utils.sumOfArrayObject(collectedAmount);
}

async function getCustomersStatisticData(supplier = null) {
  // console.log("Supplier : ", supplier);
  if (supplier) return await Customer.find({ supplier: supplier });
  return await Customer.find().lean();
}

module.exports = router;
