const userSchema = require("./model");
const mongoose = require("mongoose");
const express = require("express");
const router = express.Router();

var User = mongoose.model("User", userSchema);
console.log("USER MODULE CALLING");
router.post("/", async (req, res) => {
  console.log("LOGIN MODULE CALLING : ");
  const checkUserAlreadyRegister = await User.findOne({
    email: req.body.email
  });
  if (checkUserAlreadyRegister) {
    res.status(400).send("User already exists");
  }
  const loginData = new User(req.body);
  const loginResponse = await loginData.save();
  console.log("LOGIN : ", loginResponse);
  res.send(loginResponse);
});

module.exports = router;
