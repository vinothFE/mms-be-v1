var mongoose = require("mongoose");

var userSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true,
    min: 1,
    max: 1024,
    unique: true
  },
  password: {
    type: String,
    required: true,
    min: 5,
    max: 1024
  }
});

module.exports = userSchema;
