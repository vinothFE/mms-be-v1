var mongoose = require("mongoose");

const invoiceSchema = new mongoose.Schema({
  due: [
    {
      timestamp: { type: Date, default: new Date().now },
      amount: Number
    }
  ],
  history: [
    {
      timestamp: { type: Date, default: new Date().now },
      amount: Number
    }
  ]
});

const invoiceModel = mongoose.model("invoices", invoiceSchema);

module.exports = invoiceModel;

/* 
    - get last payment object for current month payment calculation
    - sum ( previous month amount + due - extra )
    -    1000
    - (-) 900
    -     100
*/
