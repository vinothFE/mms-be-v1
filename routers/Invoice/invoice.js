const customerSchema = require("../customers/model");
const mongoose = require("mongoose");
const Customer = mongoose.model("customers", customerSchema);
const router = require("express").Router();
const _config = require("../../config");
const _utils = require("../../utils");

// const Invoice = require("./model"); feature use
/* 
    - get current month number of days
    - get current month milk amount of the customer
    - calculate current month invoice of the customer (here need to implement like transaction method(withdraw and deposit))
    - 
*/

router.post("/update_invoice", (req, res) => {
  customerDocId = req.body.customerDocId;
  Customer.find({ _id: customerDocId }, (err, customerDoc) => {
    if (err) res.status(400).send(err);
    customerDoc = customerDoc[0];
    console.log("BWfore", customerDoc);
    customer = updateMonthlyAmount(
      customerDoc,
      req.body.amount,
      req.body.mistake_reason ? req.body.mistake_reason : ""
    );
    console.log("AFter", customer);
    Customer.findOneAndUpdate(
      { _id: customer._id },
      customer,
      { new: true },
      (err, data) => {
        if (err) res.status(400).send(err);
        data.invoice.due = data.invoice.due[0].amount;
        res.send(data);
      }
    );
  });
});

function updateMonthlyAmount(customer, milkAmount, mistake_reason) {
  dueAmount = calculateMilkAmount(
    milkAmount,
    customer.invoice.due.length ? customer.invoice.due[0] : 0
  );
  customer.invoice.is_paid = true;
  customer.invoice.due.unshift({
    timestamp: new Date().getTime(),
    amount: mistake_reason ? 0 : dueAmount,
    mistake_reason: mistake_reason
  });
  customer.invoice.history.unshift({
    timestamp: new Date().getTime(),
    amount: milkAmount,
    mistake_reason: mistake_reason
  });
  return customer;
}

router.get("/get_invoice", (req, res) => {
  Customer.find((err, customers) => {
    if (err) res.status(400).send(err);
    updatedInvoiceData = getUpdatedInvoice(customers);
    res.send(updatedInvoiceData);
  });
});

function getUpdatedInvoice(customers) {
  return customers.map(custmer => {
    custmer.invoice.due = custmer.invoice.due[0];
    return custmer;
  });
}

router.get("/create_invoice", async (req, res) => {
  try {
    Customer.find((err, customerList) => {
      if (err) res.send(err);
      customerList.forEach(customer => {
        milkAmount = getCurrentMonthAmount(customer);
        // console.log("milkAmount", milkAmount);
        // exist customer invoice update
        if (customer.invoice.due.length) {
          customer = updateInvoice(customer, milkAmount);
          // console.log("VNIO", customer);
          // Customer.update({ _id: customer._id }, customer, { new: false });
          // Customer.update({ _id: customer._id }, customer, res => {
          //   console.log("UPdate doc", res);
          // });
          customerDoc = new Customer(customer);
          customerDoc.save(doc => {
            if (err) res.status(400).send(err);
            console.log(doc);
          });
        }
        // New customer invoice update
        else {
          invoice = {};
          invoice.is_paid = false;
          invoice.due = [];
          invoice.history = [];
          invoice.due.push({
            timestamp: new Date().getTime(),
            amount: milkAmount
          });
          console.log("--New Invoice---", invoice);
          customer["invoice"] = invoice;
          customer["extra_milk"] = {};
          customer["minus_milk"] = {};
          console.log(customer);
          customerDoc = new Customer(customer);
          customerDoc.save(err => {
            if (err) res.status(400).send(err);
          });
        }
      });
      res.send("Invoice created sucessfully");
    });
  } catch (error) {
    res.send(error);
  }
});

function updateInvoice(customer, milkAmount) {
  customerDueArray = Array.from(customer.invoice.due);
  milkAmount = calculateMilkAmount(
    milkAmount,
    customerDueArray.length ? customerDueArray[0] : 0
  );
  extraMilkAmount = calculateExtraOrMinusMilkAmount(customer.extra_milk);
  minusMilkAmount = calculateExtraOrMinusMilkAmount(customer.minus_milk);
  totalCurrentMonthMilkAmount = milkAmount + extraMilkAmount - minusMilkAmount;
  customerDueArray.push({
    timestamp: new Date().getTime(),
    amount: totalCurrentMonthMilkAmount
  });
  customer.invoice.due = customerDueArray;
  return customer;
}

function getCurrentMonthAmount(customer) {
  if (customer.customer_type && customer.customer_type.toLowerCase() === "pm")
    return customer[_utils.daysInThisMonth() + "days_amount"];
  return customer[_utils.daysInLastMonth() + "days_amount"];
}

function calculateMilkAmount(currentMonthAmount, previousMonthBalance) {
  if (previousMonthBalance) {
    currentMonthAmount += previousMonthBalance["amount"];
  }
  return currentMonthAmount;
}

function calculateExtraOrMinusMilkAmount(extraOrMinusMilkObj) {
  const lastMonth = _utils.getLastMonth();
  if (extraOrMinusMilkObj[lastMonth] && extraOrMinusMilkObj[lastMonth].length) {
    return _utils.sumOfArrayObject(extraOrMinusMilkObj[lastMonth]);
  }
  return 0;
}
module.exports = router;
