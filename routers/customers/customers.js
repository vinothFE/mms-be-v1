const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const sampleCustomerData = require("../../sample_data");

const customerSchema = require("./model");
const Customer = mongoose.model("customers", customerSchema);
const _config = require("../../config");
const _utils = require("../../utils");

// get customers list
router.get("/customers", (req, res) => {
  getCustomers().then((err, data) => {
    if (err) return res.status(400).send(err);
    if (data) {
      updateCustomerList = getDueAndPaymentHistory(data);
      res.send(updateCustomerList);
    }
  });
});

// get single customer
router.get("/customer/:id", (req, res) => {
  console.log("SUto /n", req.param.id, req.params.id);
  Customer.findById({ _id: req.params.id }, (err, result) => {
    if (err) res.status(400).send("Not find a customer");
    res.send(result);
  });
});

// create and update customer
router.post("/customer", async (req, res) => {
  console.log("req.body : ", req.body);
  customerData = req.body;
  if (customerData["_id"]) {
    Customer.findByIdAndUpdate(
      { _id: customerData["_id"] },
      customerData,
      { new: true },
      (err, data) => {
        if (err) {
          res.send(err);
        }
        res.send(data);
      }
    );
  } else {
    result = createCustomerBasedOnSupplierAndLinenumberAndOrder(customerData);
    res.send(result);
  }
});

// delete customer
router.delete("/customer/:id", (req, res) => {
  console.log("req : ", req.params.id);
  Customer.findByIdAndDelete({ _id: req.body.id }, (err, data) => {
    if (err) {
      res.send(err);
    }
    res.send(data);
  });
});

/* create customers from speadsheet
 - Get spreadsheet filled data in json 
 - copy and paste in sample_data.js
 - then call this function
 - It will create customers in database
*/
router.createSampleCustomer = () => {
  console.log("createSampleCustomer calling");
  sampleCustomerData.forEach(async element => {
    const isExistCustomer = await Customer.findOne({
      customer_id: element.customer_id
    });
    if (!isExistCustomer) {
      element["invoice"] = {
        is_paid: false,
        due: [],
        history: []
      };
      element["extra_milk"] = {};
      element["minus_milk"] = {};
      console.log("New Customer!", element);
      const createCustomer = new Customer(element);
      createCustomer.save();
    }
  });
};

async function getCustomers() {
  return await Customer.find();
}

// construct due and lastPayment details from customer json
function getDueAndPaymentHistory(customerList) {
  return customerList.map(customer => {
    customer["dueAmount"] = customer.invoice.due[0].amount;
    customer["lastPaymentAmount"] = customer.invoice.history[0].amount;
  });
}

/*
  Creating new customer based on order of the customer, supplier and line number in db bucket
*/
function createCustomerBasedOnSupplierAndLinenumberAndOrder(newCustomer) {
  newCustomerDoc = new Customer(newCustomer);
  newCustomerDoc.save((err, data) => {
    newCustomer = data;
    oFlag = false;
    upArr = [];
    getCustomers().then((data, err) => {
      if (err) {
        return;
      }
      console.log("NEW CUSTOMER", newCustomer);
      if (data) {
        cArr = data;
        cArr
          .filter(val => val.supplier === newCustomer.supplier)
          .filter(val => val.line === newCustomer.line)
          .sort((a, b) => a.order - b.order)
          .map(val => {
            if (val.order === newCustomer.order) {
              upArr.push(newCustomer);
              console.log("I AM EQUAL", newCustomer, "oFlag", oFlag);
              oFlag = true;
            }
            if (oFlag) {
              val.order++;
              upArr.push(val);
            } else {
              upArr.push(val);
            }
          });

        if (!oFlag) upArr.push(newCustomer);
        upArr.forEach(customer => {
          Customer.findByIdAndUpdate({ _id: customer["_id"] }, customer, {
            new: true
          });
        });
      }
      return upArr;
    });
  });
}

// Update extra and minus milk in customer db
router.post(
  "/customer/extra-and-minus-milk",
  ({ body: { id, operation, amount } }, res) => {
    Customer.findById({ _id: id }, (err, result) => {
      if (err) {
        res.send(err);
      }
      console.log("result : ", { id, operation, amount });

      customerData = {};
      updateCustomerData = {};
      if (!result["extra_milk"]) {
        result["extra_milk"] = {};
      }
      if (!result["minus_milk"]) {
        result["minus_milk"] = {};
      }
      if (operation === "extra_amount") {
        customerData = updateExtraOrMinusMilk(
          { id, operation, amount },
          result["extra_milk"]
        );
        updateCustomerData["extra_milk"] = customerData;
      } else {
        customerData = updateExtraOrMinusMilk(
          { id, operation, amount },
          result["minus_milk"]
        );
        updateCustomerData["minus_milk"] = customerData;
      }
      console.log("customerData : ", customerData);
      Customer.findByIdAndUpdate(
        { _id: result["_id"] },
        updateCustomerData,
        { new: true },
        (err, data) => {
          if (err) {
            res.send(err);
          }
          res.send(data);
        }
      );
    });
  }
);

// Create json key current month date
function updateExtraOrMinusMilk(extraMinusMilk, customerData) {
  console.log(_utils);
  if (!customerData[_utils.getMonthAndYear()]) {
    customerData[_utils.getMonthAndYear()] = [];
  }
  customerData[_utils.getMonthAndYear()].push({
    amount: extraMinusMilk["amount"],
    timestamp: _utils.currentTimeStamp()
  });
  return customerData;
}

module.exports = router;
