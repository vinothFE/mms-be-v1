const mongoose = require("mongoose");

const customerSchema = new mongoose.Schema({
  insert_at: { type: Date, default: Date.now },
  type: { type: String, default: "customer" },
  customer_id: String,
  name: String,
  supplier: String,
  line: String,
  order: Number,
  milk_count: String,
  one_day_amount: String,
  payment_type: String,
  customer_type: String,
  "30days_amount": Number,
  "31days_amount": Number,
  "29days_amount": Number,
  "28days_amount": Number,
  start_date: Date,
  end_date: Date,
  information: String,
  invoice: {
    is_paid: Boolean,
    due: [
      {
        timestamp: { type: Date, default: Date.now() },
        amount: Number,
        mistake_reason: String
      }
    ],
    history: [
      {
        timestamp: { type: Date, default: Date.now() },
        amount: Number,
        mistake_reason: String
      }
    ]
  },
  extra_milk: {},
  minus_milk: {}
});

module.exports = customerSchema;
