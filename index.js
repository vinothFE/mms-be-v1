const express = require("express");
const app = express();
const mongoose = require("mongoose");
const _config = require("./config");
const startup = require("./startup/prod")(app);
var cors = require("cors");

// routes
const customer = require("./routers/customers/customers");
const user = require("./routers/user/user");
const invoice = require("./routers/Invoice/invoice");
const statistics = require("./routers/statistics/statistics");
const mlabPassword = "mlab@1234";
// mongoose connection
// mongoose
// .connect("mongodb://localhost/mms")
// .connect(`mongodb://vinoth:${mlabPassword}@ds125945.mlab.com:25945/mms`)
// .then(() => console.log("Connected with mongoDB"))
// .catch(err => console.log("Error on connection : ", err.message));

let mongo_mlab_db_url =
  "mongodb://vinoth:" +
  encodeURIComponent(process.env.MLAB_PASSWORD) +
  process.env.MLAB_API_KEY;
// encodeURIComponent("mms@1234") +
// "@ds125945.mlab.com:25945/mms";
console.log("MLAB MONGO : ", mongo_mlab_db_url);
let mongoDB = mongo_mlab_db_url;
mongoose.connect(mongoDB, { useNewUrlParser: true });
mongoose.Promise = global.Promise;
let db = mongoose.connection;
db.on("error", console.error.bind(console, "MongoDB connection error:"));

app.use(express.json());
app.use(cors()); // Use this after the variable declaration
app.use(_config.CUSTOMER_API, customer);
app.use(_config.USER_API, user);
app.use(_config.INVOICE_API, invoice);
app.use(_config.STATISTICS_API, statistics);

app.get("/", (req, res) => {
  res.send(" hey i am working!!!");
});

// create customer document
customer.createSampleCustomer();

const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening : ${port}`));
